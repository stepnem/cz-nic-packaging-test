#!/bin/sh

repetitions=${1:-1}

greeting="Have a nice $(date +%A), $(id -u -n)!"

i=$repetitions

while [ "$i" -gt 0 ]
do
  echo "${greeting}"
  i=$((i-1))
done
