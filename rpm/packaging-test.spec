Name:          packaging-test
Version:       0.2
Release:       1%{?dist}
Summary:       CZ.NIC packaging test

License:       GPLv3+
URL:           https://gitlab.labs.nic.cz/knot/%{name}/
Source0:       https://gitlab.labs.nic.cz/knot/packaging-test/-/archive/v0.2/%{name}-v%{version}.tar.gz

BuildRequires: gcc
BuildRequires: libuv-devel
BuildRequires: systemd-rpm-macros

%global binary demo_libuv
%global unit_file packaging-test.service

%description
CZ.NIC packaging test.

%prep
%setup -q -n %{name}-v%{version}

%build
gcc %{optflags} -luv -o %{binary} %{binary}.c
cat > %{unit_file} <<EOF
[Unit]
Description=Run %{binary}

[Service]
Type=oneshot
ExecStart=%{_bindir}/%{binary}

[Install]
WantedBy=multi-user.target
EOF

%install
mkdir -p %{buildroot}/%{_bindir}
install -m 755 %{binary} %{buildroot}/%{_bindir}/%{binary}
mkdir -p %{buildroot}/%{_unitdir}
install -m 644 %{unit_file} %{buildroot}/%{_unitdir}/%{unit_file}

%files
%license LICENSE
%{_bindir}/%{binary}
%{_unitdir}/%{unit_file}

%changelog
* Sat Jun 27 2020 Štěpán Němec <stepnem@gmail.com> - 0.2-1
- Initial release
